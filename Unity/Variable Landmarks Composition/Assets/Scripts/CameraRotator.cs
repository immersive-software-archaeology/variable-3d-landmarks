using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{

    public Vector3 CameraFocusCenter = Vector3.up;

    public float CameraRotationSpeed = 10f;

    public float CameraHeight = 1f;
    public float CameraOrbit = 5f;



    void Update()
    {
        gameObject.transform.position = new Vector3(Mathf.Cos((Time.time * CameraRotationSpeed % 360f) * Mathf.Deg2Rad), CameraHeight / CameraOrbit, Mathf.Sin((Time.time * CameraRotationSpeed % 360f) * Mathf.Deg2Rad)) * CameraOrbit;
        gameObject.transform.rotation = Quaternion.LookRotation(CameraFocusCenter - gameObject.transform.position, Vector3.up);
    }

}
