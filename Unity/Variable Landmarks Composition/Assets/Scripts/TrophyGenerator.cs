using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrophyGenerator : MonoBehaviour
{

    public TMP_InputField seedInputField;
    public Button button;

    private readonly bool debug = true;

    private readonly HttpClient client = new HttpClient();

    private readonly string ROOT_NAME = "Root";
    private readonly string HOOK_PREFIX = "Hook";
    private readonly string HOOK_CONNECTOR = "To";
    private readonly string MATERIAL_PREFIX = "Material";

    public Transform PartialGeometryPrefab;
    public string MaterialBaseFolderPath = "Materials/";

    private List<string> selectedFeatures;



    void Start()
    {
        button.onClick.AddListener(delegate { GenerateNewModel(); });
    }

    private void GenerateNewModel()
    {
        Task<string> task = Task<string>.Run(async () => await GetConfigurationFromEclipse());

        if (task.Result != null && task.Result.Trim().Length > 0)
            startGeneration(task.Result);
        else
            Debug.LogError("Invalid Request");
    }

    private async Task<string> GetConfigurationFromEclipse()
    {
        string configurationString;
        try
        {
            configurationString = await client.GetStringAsync("http://localhost:9005/configuration/?seed=" + seedInputField.text);
            return configurationString;
        }
        catch (HttpRequestException e)
        {
            Debug.LogError(e);
            return null;
        }
    }



    private void startGeneration(string configurationString)
    {
        if (debug)
        {
            Debug.Log("Starting trophy generation process");
            Debug.Log("Configuration: " + configurationString);
        }
        selectedFeatures = new List<string>(configurationString.Split(","));

        // Clear all previously generated trophies...
        foreach (Transform child in gameObject.transform)
            GameObject.Destroy(child.gameObject);

        //PartialGeometryPrefab = Instantiate(Resources.Load("Models") as GameObject).transform;

        List<Transform> rootTransforms = getTransformBasedOnNamePrefix(PartialGeometryPrefab, HOOK_PREFIX + "_" + ROOT_NAME, false);
        if (rootTransforms.Count != 1)
        {
            Debug.LogError("There must be exactly one root transform, found " + rootTransforms.Count);
            return;
        }

        Transform rootTransform = Duplicate(rootTransforms[0]);
        if (rootTransform == null)
        {
            Debug.LogError("Could not find the root model hook!");
            return;
        }
        rootTransform.parent = gameObject.transform;
        rootTransform.transform.position = Vector3.zero;

        placeObjectIntoHook(rootTransform, ROOT_NAME);
        applyMaterialChanges(rootTransform);

        //GameObject.Destroy(PartialGeometryPrefab.gameObject);
    }



    private void placeObjectIntoHook(Transform parentHook, string parentName)
    {
        if (debug)
            Debug.Log("Placing new Object at hook \"" + parentHook.name + "\" in parent \"" + parentName + "\".");

        string nextFeatureNamePrefix = getNextFeatureNamePrefix(parentHook.name, parentName);
        List<Transform> nextFeatureTransforms = getTransformBasedOnNamePrefix(PartialGeometryPrefab, nextFeatureNamePrefix, true);
        if (nextFeatureTransforms.Count == 0)
        {
            if (debug)
                Debug.Log("Hook is meant for an optional feature that is not contained in the selection");
            return;
        }
        else if (nextFeatureTransforms.Count > 1)
        {
            Debug.LogError("More than one 3D model found for given feature prefix \"" + nextFeatureNamePrefix + "\"");
            return;
        }

        Transform modelTransform = Duplicate(nextFeatureTransforms[0]);
        modelTransform.parent = parentHook;
        modelTransform.localPosition = Vector3.zero;
        modelTransform.localRotation = Quaternion.identity;
        modelTransform.localScale = Vector3.one;

        foreach (Transform childHook in getTransformBasedOnNamePrefix(modelTransform, HOOK_PREFIX, false))
        {
            placeObjectIntoHook(childHook, modelTransform.name);
        }
    }

    private Transform Duplicate(Transform original)
    {
        Transform duplicate = Instantiate(original);
        if (duplicate.name.EndsWith("(Clone)"))
            duplicate.name = duplicate.name.Substring(0, duplicate.name.Length - "(Clone)".Length);
        return duplicate;
    }

    private List<Transform> getTransformBasedOnNamePrefix(Transform transformToSearch, string prefix, bool checkConfiguration)
    {
        List<Transform> result = new List<Transform>();
        foreach (Transform modelPart in transformToSearch)
        {
            if (modelPart.name.StartsWith(prefix))
            {
                if (checkConfiguration && !selectedFeatures.Contains(modelPart.name))
                    continue;
                result.Add(modelPart);
            }
        }
        return result;
    }

    private string getNextFeatureNamePrefix(string parentHookName, string parentFeatureName)
    {
        string nextFeatureNamePrefix = parentHookName;
        if (!nextFeatureNamePrefix.StartsWith(HOOK_PREFIX))
            throw new Exception("Hook names must start with \"" + HOOK_PREFIX + "\"");

        nextFeatureNamePrefix = nextFeatureNamePrefix.Substring(HOOK_PREFIX.Length + 1);
        if (!nextFeatureNamePrefix.StartsWith(parentFeatureName))
            throw new Exception("Hook name did not contain previous feature name \"" + parentFeatureName + "\"");

        nextFeatureNamePrefix = nextFeatureNamePrefix.Substring(parentFeatureName.Length + 1);
        if (!nextFeatureNamePrefix.StartsWith(HOOK_CONNECTOR))
            throw new Exception("Hook name must connect feature name and next feature prefix with \"" + HOOK_CONNECTOR + "\"");

        nextFeatureNamePrefix = nextFeatureNamePrefix.Substring(HOOK_CONNECTOR.Length + 1);

        string[] segments = nextFeatureNamePrefix.Split("_");
        if (int.TryParse(segments[segments.Length - 1], out _))
        {
            List<string> newSegments = new List<string>(segments);
            newSegments.RemoveAt(newSegments.Count - 1);
            return string.Join("_", newSegments);
        }

        return nextFeatureNamePrefix;
    }



    private void applyMaterialChanges(Transform transform)
    {
        List<Material> newMaterials = new List<Material>();
        foreach (string selectedFeature in selectedFeatures)
        {
            if (selectedFeature.StartsWith(MATERIAL_PREFIX))
            {
                if (selectedFeature.Split("_").Length < 2)
                    continue;

                if (debug)
                    Debug.Log("Fetching material: " + selectedFeature);

                Material mat = Resources.Load(MaterialBaseFolderPath + selectedFeature.Trim()) as Material;
                if (mat == null)
                {
                    Debug.LogError("Material \"" + MaterialBaseFolderPath + selectedFeature + "\" could not be found");
                    continue;
                }

                newMaterials.Add(mat);

                if (debug)
                    Debug.Log("Fetched material: " + mat.name);
            }
        }

        doApplyMaterialChanges(transform, newMaterials);
    }
    private void doApplyMaterialChanges(Transform transform, List<Material> newMaterials)
    {
        if (transform.TryGetComponent<Renderer>(out Renderer renderer))
        {
            if (debug)
                Debug.Log("Setting materials to \"" + transform.name + "\"");

            Material[] matsToSet = new Material[renderer.materials.Length];

            for (int oldMatIndex = 0; oldMatIndex < renderer.materials.Length; oldMatIndex++)
            {
                Material oldMat = renderer.materials[oldMatIndex];
                string[] oldMatNameSegments = oldMat.name.Split("_");
                foreach (Material newMat in newMaterials)
                {
                    string[] newMatNameSegments = newMat.name.Split("_");

                    bool missmatch = false;
                    for (int i = 0; i < 2; i++)
                    {
                        if (!oldMatNameSegments[i].Equals(newMatNameSegments[i]))
                        {
                            missmatch = true;
                            break;
                        }
                    }
                    if (missmatch)
                        continue;

                    matsToSet[oldMatIndex] = newMat;
                    break;
                }
            }

            for (int i = 0; i < matsToSet.Length; i++)
            {
                if (matsToSet[i] == null)
                {
                    matsToSet[i] = renderer.materials[i];
                }
            }

            renderer.materials = matsToSet;
        }

        foreach (Transform child in transform)
        {
            doApplyMaterialChanges(child, newMaterials);
        }
    }
}