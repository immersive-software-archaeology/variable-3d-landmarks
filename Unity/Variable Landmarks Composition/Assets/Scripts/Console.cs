using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Console : MonoBehaviour
{

    public Color MetaColor = Color.gray;

    public Color AssertColor = Color.blue;
    public Color ErrorColor = Color.red;
    public Color ExceptionColor = Color.red;
    public Color LogColor = Color.white;
    public Color WarningColor = Color.yellow;

    private string text = "";

    [SerializeField]
    private TMP_Text Text;



    void Start()
    {
        Text.text = text;
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }



    public void HandleLog(string logString, string stackTrace, LogType type)
    {
        string metaColor = "#" + ColorUtility.ToHtmlStringRGBA(MetaColor);

        string fontColor;
        if (type == LogType.Assert)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(AssertColor);
        else if (type == LogType.Error)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(ErrorColor);
        else if (type == LogType.Exception)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(ExceptionColor);
        else if (type == LogType.Log)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(LogColor);
        else if (type == LogType.Warning)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(WarningColor);
        else
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(Color.magenta);

        text += ("<color=" + metaColor + ">[" + Mathf.Round(Time.time * 10) / 10f + "]</color> <color=" + fontColor + ">" + logString + "</color>") + "\n";
        Text.text = text;

        Text.GetComponent<RectTransform>().sizeDelta = new Vector2(0, Text.preferredHeight);
    }
}
