# Variable 3D Trophies

This example project demonstrates how to
- use a feature model to define the variability space of a 3D landmark (trophy),
- sample configurations from it using a seed,
- compose the 3D structure according to the samples configuration.

![Short Video Preview](preview.gif)



&nbsp;



## Contact

In case you have questions, please don't hesitate to contact me: [adho@itu.dk](adho@itu.dk).



&nbsp;
