package dk.itu.cs.isa.variability;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import de.ovgu.featureide.fm.core.analysis.cnf.CNF;
import de.ovgu.featureide.fm.core.analysis.cnf.LiteralSet;
import de.ovgu.featureide.fm.core.analysis.cnf.SolutionList;
import de.ovgu.featureide.fm.core.analysis.cnf.formula.FeatureModelFormula;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.RandomConfigurationGenerator;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.init.FMCoreLibrary;
import de.ovgu.featureide.fm.core.io.csv.ConfigurationListFormat;
import de.ovgu.featureide.fm.core.io.manager.FeatureModelManager;
import de.ovgu.featureide.fm.core.io.manager.FileHandler;
import de.ovgu.featureide.fm.core.job.LongRunningWrapper;
import de.ovgu.featureide.fm.core.job.monitor.ConsoleMonitor;

public class ISAFeatureModelSampler {
	
	public List<String> createSample(long seed) throws Exception {
		FMCoreLibrary.getInstance().install();
		
		// For later use in plugin: see dk.itu.cs.isa.util
//		Path fmFile = FileUtil.getFileFromPlugin("dk.itu.cs.isa.variability", "model/model.xml").toPath();
		
		Path fmFile = new File("model/model.xml").toPath();
		Path outputFile = new File("model/output.txt").toPath();
		
		final FileHandler<IFeatureModel> fileHandler = FeatureModelManager.getFileHandler(fmFile);
		if (fileHandler.getLastProblems().containsError()) {
			throw new IllegalArgumentException(fileHandler.getLastProblems().getErrors().get(0).error);
		}
		final CNF cnf = new FeatureModelFormula(fileHandler.getObject()).getCNF();
		
		RandomConfigurationGenerator generator = new RandomConfigurationGenerator(cnf, 1);
		generator.getRandom().setSeed(seed);
		final List<LiteralSet> result = LongRunningWrapper.runMethod(generator, new ConsoleMonitor<>());

		SolutionList solutionList = new SolutionList(cnf.getVariables(), result);
		FileHandler.save(outputFile, solutionList, new ConfigurationListFormat());

		List<String> selectedFeatures = new ArrayList<>();
		for(LiteralSet solutionSet : solutionList.getSolutions()) {
			solutionSet = solutionSet.getPositive();
			
			for(int selectedLiteral : solutionSet.getLiterals()) {
				String selectedFeatureName = solutionList.getVariables().getName(selectedLiteral);
				selectedFeatures.add(selectedFeatureName);
			}
		}
		return selectedFeatures;
	}
	
}
