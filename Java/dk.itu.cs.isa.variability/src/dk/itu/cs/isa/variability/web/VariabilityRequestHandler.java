package dk.itu.cs.isa.variability.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import dk.itu.cs.isa.variability.ISAFeatureModelSampler;

public class VariabilityRequestHandler extends AbstractHandler {

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if(target.equals("/configuration/"))
			handle(baseRequest, response);
	}

	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		long numberOfSamples;
		try {
			numberOfSamples = Long.parseLong(baseRequest.getParameter("seed"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		ISAFeatureModelSampler sampler = new ISAFeatureModelSampler();
		List<String> featuresInSamples;
		try {
			featuresInSamples = sampler.createSample(numberOfSamples);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		response.setContentType("application/json; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);

		PrintWriter out = response.getWriter();
		out.println(String.join(",", featuresInSamples));
		out.close();

		baseRequest.setHandled(true);
	}

	@SuppressWarnings("unused")
	private String printSamples(List<List<String>> samples) {
		List<String> featureNameLists = new ArrayList<>(samples.size());
		for(List<String> featureNames : samples) {
			featureNameLists.add(String.join(",", featureNames));
		}
		return String.join("\n", featureNameLists);
	}

}
