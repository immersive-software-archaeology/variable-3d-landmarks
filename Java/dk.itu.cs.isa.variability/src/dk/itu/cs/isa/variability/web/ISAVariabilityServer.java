package dk.itu.cs.isa.variability.web;

import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;

public class ISAVariabilityServer {

	private Server jettyServer;

	public ISAVariabilityServer() {
		Runnable serverRunnable = new Runnable() {
			@Override
			public void run() {
				jettyServer = new Server(9005);
				jettyServer.getConnectors()[0].getConnectionFactory(HttpConnectionFactory.class);
				jettyServer.setHandler(new VariabilityRequestHandler());
				try {
					jettyServer.start();
					jettyServer.join();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		new Thread(serverRunnable).start();
	}
	
}
