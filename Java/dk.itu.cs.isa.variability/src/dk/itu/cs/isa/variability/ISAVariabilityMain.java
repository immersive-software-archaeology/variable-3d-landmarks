package dk.itu.cs.isa.variability;

import dk.itu.cs.isa.variability.web.ISAVariabilityServer;

public class ISAVariabilityMain {
	
	public static void main(String[] args) {
		try {
			new ISAVariabilityServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
